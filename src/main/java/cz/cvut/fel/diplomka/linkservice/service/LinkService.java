package cz.cvut.fel.diplomka.linkservice.service;

import cz.cvut.fel.diplomka.linkservice.model.DomainLinksInfo;
import cz.cvut.fel.diplomka.linkservice.model.Link;
import cz.cvut.fel.diplomka.linkservice.model.PopularDomains;
import cz.cvut.fel.diplomka.linkservice.model.robots.LinkRecord;
import cz.cvut.fel.diplomka.linkservice.model.robots.LinksForDomain;
import cz.cvut.fel.diplomka.linkservice.repository.DomainLinksInfoRepository;
import cz.cvut.fel.diplomka.linkservice.repository.LinkRepository;
import cz.cvut.fel.diplomka.linkservice.repository.PopularDomainsRepository;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class LinkService {

    private final LinkRepository linkRepository;
    private final DomainLinksInfoRepository domainLinksInfoRepository;
    private final PopularDomainsRepository popularDomainsRepository;

    @Autowired
    public LinkService(LinkRepository linkRepository, DomainLinksInfoRepository domainLinksInfoRepository, PopularDomainsRepository popularDomainsRepository) {
        this.linkRepository = linkRepository;
        this.domainLinksInfoRepository = domainLinksInfoRepository;
        this.popularDomainsRepository = popularDomainsRepository;
    }

    @RabbitListener(queues = "#{domainLinksQueue.name}")
    public void findDomainLinks(LinksForDomain message) {
        for (LinkRecord linkRecord : message.links()){
            Link link = new Link(LocalDateTime.parse(linkRecord.foundAt()), linkRecord.linkTo(), message.domain(), linkRecord.title());
            linkRepository.save(link);
        }
    }

    public List<DomainLinksInfo> getPopularDomains(boolean forceReload) {
        LocalDate today = LocalDate.now();
        Optional<PopularDomains> popularDomainsOptional = popularDomainsRepository.findById(today.toString());
        if (popularDomainsOptional.isPresent() && !forceReload) {
            return popularDomainsOptional.get().getValue();
        } else {
            List<DomainLinksInfo> result = domainLinksInfoRepository.findTop50Domains(today);
            popularDomainsRepository.save(new PopularDomains(today.toString(), result));
            return result;
        }
    }
}
