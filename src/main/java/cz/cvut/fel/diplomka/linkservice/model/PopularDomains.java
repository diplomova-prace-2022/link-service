package cz.cvut.fel.diplomka.linkservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@RedisHash(value = "PopularDomains", timeToLive = 60 * 60 * 48L)
public class PopularDomains implements Serializable {

    @Id
    private String date;
    private List<DomainLinksInfo> value = new ArrayList<>();

}
