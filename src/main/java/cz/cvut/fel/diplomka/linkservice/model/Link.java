package cz.cvut.fel.diplomka.linkservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Data
@AllArgsConstructor
@Table("links")
public class Link implements Serializable {

    @PrimaryKey
    private LinkKey pk;

    @Column("text")
    private String text;

    public Link(LocalDate date, String target, String source, LocalTime time, String text) {
        this.pk = new LinkKey(date, target, source, time);
        this.text = text;
    }

    public Link(LocalDateTime datetime, String target, String source, String text) {
        this.pk = new LinkKey(datetime.toLocalDate(), target, source, datetime.toLocalTime());
        this.text = text;
    }

}
