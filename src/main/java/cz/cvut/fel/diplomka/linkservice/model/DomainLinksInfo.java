package cz.cvut.fel.diplomka.linkservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.io.Serializable;

@Data
@AllArgsConstructor
@Table("link_domain_aggregation")
public class DomainLinksInfo implements Serializable {

    @PrimaryKey
    private DomainLinkInfoKey pk;

}
