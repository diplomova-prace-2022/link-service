package cz.cvut.fel.diplomka.linkservice.repository;

import cz.cvut.fel.diplomka.linkservice.model.DomainLinkInfoKey;
import cz.cvut.fel.diplomka.linkservice.model.DomainLinksInfo;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface DomainLinksInfoRepository extends CassandraRepository<DomainLinksInfo, DomainLinkInfoKey> {

    @Query(value = "SELECT * FROM link_domain_aggregation WHERE date=:date ORDER BY incoming_links_count DESC LIMIT 50")
    List<DomainLinksInfo> findTop50Domains(@Param("date") LocalDate date);

}
