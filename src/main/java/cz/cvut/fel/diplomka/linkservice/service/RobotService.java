package cz.cvut.fel.diplomka.linkservice.service;

import cz.cvut.fel.diplomka.linkservice.model.robots.FindDomainLinksTask;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class RobotService {

    private final RabbitTemplate rabbitTemplate;
    private final TopicExchange robotExchange;
    private final DomainService domainService;

    @Autowired
    public RobotService(RabbitTemplate rabbitTemplate, @Qualifier("robotExchange") TopicExchange robotExchange, DomainService domainService) {
        this.rabbitTemplate = rabbitTemplate;
        this.robotExchange = robotExchange;
        this.domainService = domainService;
    }

    @Scheduled(fixedDelayString = "${robots.task.fixedDelay}", initialDelayString = "${robots.task.initialDelay}")
    public void createFindDomainLinksTask() {
        log.info("Sending task 'Find domain links'");
        rabbitTemplate.convertAndSend(robotExchange.getName(), "robot.task.links.domain", new FindDomainLinksTask(domainService.getRandomDomain()));
    }

}
