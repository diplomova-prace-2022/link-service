package cz.cvut.fel.diplomka.linkservice.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/links/admin")
public class SystemAdminController implements ApplicationContextAware {

    private ApplicationContext context;

    @Value("${my.favourite.animal}")
    private String animal;

    @GetMapping("/health")
    public ResponseEntity<String> healthCheck(){
        log.trace("Health request arrived. Checking if my pulse...");
        return ResponseEntity.ok("Still alive :-)");
    }

    @GetMapping("/info")
    public ResponseEntity<String> info(){
        return ResponseEntity.ok(System.getenv("HOSTNAME"));
    }

    @PostMapping("/shutdown")
    public void shutdown(){
        log.info("Shutting down application context...");
        ((ConfigurableApplicationContext) context).close();
    }

    @PostMapping("/error")
    public void throwError(){
        throw new RuntimeException("Mysterious exception thrown on purpose");
    }

    @GetMapping("/favourite-animal")
    public ResponseEntity<String> getAnimal(){
        return ResponseEntity.ok(this.animal);
    }

    @Override
    public void setApplicationContext(ApplicationContext ctx) throws BeansException {
        this.context = ctx;
    }
}
