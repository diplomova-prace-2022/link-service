package cz.cvut.fel.diplomka.linkservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class LinkServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(LinkServiceApplication.class, args);
	}

}
