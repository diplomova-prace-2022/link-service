package cz.cvut.fel.diplomka.linkservice.model.robots;

import java.io.Serializable;

public record LinkRecord(String linkTo, String title, String foundAt) implements Serializable {
}
