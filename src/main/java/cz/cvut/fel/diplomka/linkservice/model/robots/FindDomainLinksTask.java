package cz.cvut.fel.diplomka.linkservice.model.robots;

import java.io.Serializable;

public record FindDomainLinksTask(String domain) implements Serializable {
}
