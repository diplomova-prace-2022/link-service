package cz.cvut.fel.diplomka.linkservice.repository;

import cz.cvut.fel.diplomka.linkservice.model.PopularDomains;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface PopularDomainsRepository extends CrudRepository<PopularDomains, String> {
}
