package cz.cvut.fel.diplomka.linkservice.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class LinkAnalysisService {

    private final RabbitTemplate rabbitTemplate;
    private final TopicExchange analysisExchange;

    @Autowired
    public LinkAnalysisService(RabbitTemplate rabbitTemplate, TopicExchange analysisExchange) {
        this.rabbitTemplate = rabbitTemplate;
        this.analysisExchange = analysisExchange;
    }

    @Scheduled(fixedDelayString = "${analysis.task.fixedDelay}", initialDelayString = "${analysis.task.initialDelay}")
    public void createAnalysisTask() {
        log.info("Generating new analysis task");
        rabbitTemplate.convertAndSend(analysisExchange.getName(), "analysis.links", "{}");
    }

}
