package cz.cvut.fel.diplomka.linkservice.model.convertors;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fel.diplomka.linkservice.model.DomainLinkInfoKey;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;
import org.springframework.stereotype.Component;

@Component
@WritingConverter
public class DomainLinksInfoKeyToBytesConvertor implements Converter<DomainLinkInfoKey, byte[]> {

    private final ObjectMapper objectMapper;

    @Autowired
    public DomainLinksInfoKeyToBytesConvertor(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @SneakyThrows
    @Override
    public byte[] convert(DomainLinkInfoKey source) {
        return objectMapper.writeValueAsBytes(source);
    }
}
