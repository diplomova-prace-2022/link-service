package cz.cvut.fel.diplomka.linkservice.model.robots;

import java.io.Serializable;
import java.util.List;

public record LinksForDomain(String domain, List<LinkRecord> links) implements Serializable {
}
