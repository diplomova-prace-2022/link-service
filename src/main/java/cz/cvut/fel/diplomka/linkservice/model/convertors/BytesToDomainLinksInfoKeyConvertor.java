package cz.cvut.fel.diplomka.linkservice.model.convertors;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fel.diplomka.linkservice.model.DomainLinkInfoKey;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;
import org.springframework.stereotype.Component;

@Component
@ReadingConverter
public class BytesToDomainLinksInfoKeyConvertor implements Converter<byte[], DomainLinkInfoKey> {

    private final ObjectMapper objectMapper;

    @Autowired
    public BytesToDomainLinksInfoKeyConvertor(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @SneakyThrows
    @Override
    public DomainLinkInfoKey convert(byte[] source) {
        return objectMapper.readValue(new String(source), DomainLinkInfoKey.class);
    }
}
