package cz.cvut.fel.diplomka.linkservice.rest;

import cz.cvut.fel.diplomka.linkservice.model.DomainLinksInfo;
import cz.cvut.fel.diplomka.linkservice.service.LinkService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/links")
public class LinksController {

    private final LinkService linkService;

    @Autowired
    public LinksController(LinkService linkService) {
        this.linkService = linkService;
    }

    @GetMapping(value="/popular-domains", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<DomainLinksInfo>> getPopularLinks(){
        return ResponseEntity.ok(linkService.getPopularDomains(false));
    }

    @GetMapping(value="/popular-domains-force", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<DomainLinksInfo>> getPopularLinksForceReload(){
        return ResponseEntity.ok(linkService.getPopularDomains(true));
    }

}
