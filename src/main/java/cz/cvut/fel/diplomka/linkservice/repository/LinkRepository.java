package cz.cvut.fel.diplomka.linkservice.repository;

import cz.cvut.fel.diplomka.linkservice.model.Link;
import cz.cvut.fel.diplomka.linkservice.model.LinkKey;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LinkRepository extends CassandraRepository<Link, LinkKey> {
}
