FROM openjdk:17-alpine
RUN addgroup -S my_group && adduser -S my_user -G my_group
USER my_user
COPY target/link-service*.jar /app/link-service.jar
ENTRYPOINT ["java", "-jar", "/app/link-service.jar"]
